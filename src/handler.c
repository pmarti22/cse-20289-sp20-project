/* handler.c: HTTP Request Handlers */

#include "spidey.h"

#include <errno.h>
#include <limits.h>
#include <string.h>

#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

/* Internal Declarations */
Status handle_browse_request(Request *request);
Status handle_file_request(Request *request);
Status handle_cgi_request(Request *request);
Status handle_error(Request *request, Status status);

/**
 * Handle HTTP Request.
 *
 * @param   r           HTTP Request structure
 * @return  Status of the HTTP request.
 *
 * This parses a request, determines the request path, determines the request
 * type, and then dispatches to the appropriate handler type.
 *
 * On error, handle_error should be used with an appropriate HTTP status code.
 **/
Status  handle_request(Request *r) {

    log("Start of handle_request");
    Status result;

    /* Parse request */
    result = parse_request(r);
    log("Result: %i", result);
    if(result == -1)
    {
        return handle_error(r, HTTP_STATUS_BAD_REQUEST);
    }

    /* Determine request path */
    log("Determine request path");
    r->path = determine_request_path(r->uri);
    log("R Path: %s", r->path);

    if(r->path==NULL){
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }

    /* Dispatch to appropriate request handler type based on file type */
    log("Dispatch Appropiate Request Handler");
    struct stat s;
    if(stat(r->path, &s)<0){
        debug("Unable to perform stat call (handler)");
        result = handle_error(r,HTTP_STATUS_NOT_FOUND);
    }

    else if((s.st_mode & S_IFMT) == S_IFDIR){ // directory
        log("Browse request\n");
        result = handle_browse_request(r);

    }
    else if(!access(r->path, X_OK)){ // excecutable file
        log("CGI request");
        result = handle_cgi_request(r);
    }
    else if(!access(r->path, R_OK)){//readable file
        log("file request");
        result = handle_file_request(r);
    }
    else{
        result = handle_error(r, HTTP_STATUS_NOT_FOUND);
    }


    log("HTTP REQUEST STATUS: %s", http_status_string(result));

    return result;
}

/**
 * Handle browse request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP browse request.
 *
 * This lists the contents of a directory in HTML.
 *
 * If the path cannot be opened or scanned as a directory, then handle error
 * with HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_browse_request(Request *r) {
    struct dirent **entries;
    int n;
    /* Open a directory for reading or scanning */
    n = scandir(r->path, &entries, NULL, alphasort);
    if(n<0){
        log("Error scanning directory");
        return handle_error(r, HTTP_STATUS_NOT_FOUND);
    }
    /* Write HTTP Header with OK Status and text/html Content-Type */
    fprintf(r->stream,"HTTP/1.0 200 OK\r\nContent-Type: text/html\r\n");
    fprintf(r->stream,"\r\n");
    /* For each entry in directory, emit HTML list item */
    fprintf(r->stream,"<ul>\r\n");
    for(int i = 0 ;i<n;i++){
        if(!(streq(entries[i]->d_name, "."))){ // Use streq, easier
            if(streq(r->uri,"/")){
                fprintf(r->stream, "<li><a href=\"/%s\">%s</a></li>\n", entries[i]->d_name, entries[i]->d_name);
            }
            else{
                fprintf(r->stream, "<li><a href=\"/%s/%s\">%s</a></li>\n", r->uri+1, entries[i]->d_name, entries[i]->d_name);
            }
        }
        free(entries[i]);
    }

    fprintf(r->stream,"</ul>\n");

    free(entries);
    fflush(r->stream);


    /* Return OK */
    return HTTP_STATUS_OK;
}

/**
 * Handle file request.
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This opens and streams the contents of the specified file to the socket.
 *
 * If the path cannot be opened for reading, then handle error with
 * HTTP_STATUS_NOT_FOUND.
 **/
Status  handle_file_request(Request *r) {
    FILE *fs;
    char buffer[BUFSIZ];
    char *mimetype = NULL;
    size_t nread;

    /* Open file for reading */
    log("Opening file for reading");
    fs = fopen(r->path , "r");
    //check if fopen fails
    if(fs==NULL){
        debug("Unable to fopen file: %s\n",strerror(errno));
        return handle_error(r,HTTP_STATUS_NOT_FOUND);
    }
    /* Determine mimetype */
    //log("Determining Mimetype");
    mimetype = determine_mimetype(r->path);

    if(mimetype == NULL){
        debug("Unable to determine mimetype: %s\n",strerror(errno));
        fclose(fs);
        return handle_error(r,HTTP_STATUS_BAD_REQUEST);
    }

    /* Write HTTP Headers with OK status and determined Content-Type */
    fprintf(r->stream,"HTTP/1.0 200 OK\r\nContent-Type: %s\r\n", mimetype);
    fprintf(r->stream,"\r\n");

    /* Read from file and write to socket in chunks */
    while((nread = fread(buffer,sizeof(char),BUFSIZ,fs))>0){
        if((fwrite(buffer,sizeof(char),nread,r->stream)) != nread) {
            debug("Unable to fwrite\n");
            goto fail;
        }
    }
    free(mimetype);
    fflush(r->stream);
    /* Close file, deallocate mimetype, return OK */
    fclose(fs);
    return HTTP_STATUS_OK;

fail:
    /* Close file, free mimetype, return INTERNAL_SERVER_ERROR */
    fclose(fs);
    free(mimetype);
    return HTTP_STATUS_INTERNAL_SERVER_ERROR;
}

/**
 * Handle CGI request
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP file request.
 *
 * This popens and streams the results of the specified executables to the
 * socket.
 *
 * If the path cannot be popened, then handle error with
 * HTTP_STATUS_INTERNAL_SERVER_ERROR.
 **/
Status  handle_cgi_request(Request *r) {
    FILE *pfs;
    char buffer[BUFSIZ];
    log("Handle CGI request - start");
    /* Export CGI environment variables from request:
     * http://en.wikipedia.org/wiki/Common_Gateway_Interface */
    //Pablo - check if missing a variable
    setenv("REMOTE_PORT",r->port,1);
    setenv("SCRIPT_FILENAME",r->path,1);
    setenv("QUERY_STRING",r->query,1);
    setenv("REQUEST_METHOD",r->method,1);
    setenv("REQUEST_URI",r->uri,1);
    setenv("REMOTE_ADDR",r->host,1);
    setenv("DOCUMENT_ROOT",RootPath,1);
    setenv("SERVER_PORT", Port,1);


    /* Export CGI environment variables from request headers */
    Header *h = r->headers;
    if(!h){
        debug("Unable to initialize Header\n");
    }
    else{
        log("Before entering while loop - cgi req");
        while(h != NULL){
            if(h->name == NULL)
                break;
            if(streq(h->name, "Accept-Language")){
                setenv("HTTP_ACCEPT_LANGUAGE",h->data,1);
            }
            if(streq(h->name, "Accept-Encoding")){
                setenv("HTTP_ACCEPT_ENCODING",h->data,1);
            }
            if(streq(h->name, "Accept")){
                setenv("HTTP_ACCEPT",h->data,1);
            }
            if(streq(h->name, "Host")){
                setenv("HTTP_HOST",h->data,1);
            }
            if(streq(h->name, "Connection")){
                setenv("HTTP_CONNECTION",h->data,1);
            }
            if(streq(h->name, "User-Agent")){
                setenv("HTTP_USER_AGENT",h->data,1);
            }
            h = h->next;

        }
        log("Exited while loop - cgi req");
    }

    /* POpen CGI Script */
    pfs = popen(r->path, "r");
    if(pfs == NULL){
        debug("Unable to popen\n");
        return handle_error(r,HTTP_STATUS_NOT_FOUND);
    }
    /* Copy data from popen to socket */
    while(fgets(buffer,BUFSIZ,pfs)){
        fputs(buffer,r->stream);
    }
    /* Close popen, return OK */
    pclose(pfs);
    fflush(r->stream);
    return HTTP_STATUS_OK;
}

/**
 * Handle displaying error page
 *
 * @param   r           HTTP Request structure.
 * @return  Status of the HTTP error request.
 *
 * This writes an HTTP status error code and then generates an HTML message to
 * notify the user of the error.
 **/
Status  handle_error(Request *r, Status status) {
    const char *status_string = http_status_string(status);
    if(status_string ==NULL){
        debug("http_status_string failed\n");
        return handle_error(r,HTTP_STATUS_BAD_REQUEST);
    }
    /* Write HTTP Header */
    fprintf(r->stream, "HTTP/1.0 %s\r\nContent-Type: text/html\r\n", status_string);
    fprintf(r->stream, "\r\n");
    /* Write HTML Description of Error*/
    if(status == HTTP_STATUS_NOT_FOUND){
        fprintf(r->stream,"<html><h1>%s</h1><h2>Nope, couldn't find it :(</h1></html>", status_string);
    }
    else if(status == HTTP_STATUS_BAD_REQUEST){
        fprintf(r->stream,"<html><h1>%s</h1><h2>Bad Request! :(</h2></html>", status_string);
    }
    else{
        fprintf(r->stream,"<html><h1>%s</h1><h2>Something went very wrong!</h2></html>",status_string);
    }
    fflush(r->stream);


    /* Return specified status */
    return status;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
