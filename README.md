# Project - README

This is the [Final Project] for [CSE 20289 Systems Programming (Spring 2020)].

## Members

- Pablo Martinez-Abrego (pmarti22@nd.edu)
- Estefania Romero (eromero4@nd.edu)

## Demonstration

- [Link to Demonstration Video](https://youtu.be/rlJzwTh-NzM)

## Errata

In our tests for latency and throughput, some of the numbers were quite odd
and inconsistent. We believe it might have been the student machines, but we
are unsure on what caused some odd results.

## Contributions

Constantly worked together in Zoom to debug and code the project overall.
Pablo was responsible for thor.py, spidey.c request.c, sockets.c and utils.c
Estefania was responsible for handler.c, single.c, forking.c and shell scripts
Constantly watched twitch lectures and VOH for help on certain code.


[Final Project]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp20/project.html
[CSE 20289 Systems Programming (Spring 2020)]: https://www3.nd.edu/~pbui/teaching/cse.20289.sp20/
