#!/usr/bin/env python3

import concurrent.futures
import os
import requests
import sys
import time

# Functions

def usage(status=0):
    progname = os.path.basename(sys.argv[0])
    print(f'''Usage: {progname} [-h HAMMERS -t THROWS] URL
    -h  HAMMERS     Number of hammers to utilize (1)
    -t  THROWS      Number of throws per hammer  (1)
    -v              Display verbose output
    ''')
    sys.exit(status)

def hammer(url, throws, verbose, hid):
    ''' Hammer specified url by making multiple throws (ie. HTTP requests).

    - url:      URL to request
    - throws:   How many times to make the request
    - verbose:  Whether or not to display the text of the response
    - hid:      Unique hammer identifier

    Return the average elapsed time of all the throws.
    '''

    sum = 0
    for i in range(throws):
        timeBefore = time.time();
        request = requests.get(url);
        timeTot = time.time() - timeBefore;
        if(verbose):
            print(request.text)
        output = "Hammer: " + str(hid) + ", Throw: "+ str(i) +", Elapsed Time: "+ str(round(timeTot, 2))
        print(output)
        sum = sum + timeTot

    timeAvg = sum / throws
    output = "Hammer: " + str(hid) + ", AVERAGE , Elapsed Time: " + str(round(timeAvg, 2))
    print(output)

    return timeAvg

def do_hammer(args):
    ''' Use args tuple to call `hammer` '''

    return hammer(*args)

def main():
    hammers = 1
    throws  = 1
    verbose = False

    # Parse command line arguments
    args = sys.argv[1:]
    while args and args[0].startswith('-'):
        arg = args.pop(0)
        if arg == '-h':
            try:
                hammers = int(args.pop(0))
            except ValueError:
                print("Invalid Number")
                usage()
        elif arg == '-t':
            try:
                throws = int(args.pop(0))
            except ValueError:
                print("Invalid Number")
                usage()
        elif arg == '-v':
            verbose = True;

    if len(args) == 0:
        usage(1)
    else:
        URL = args.pop(0)

    list = []
    for i in range(hammers):
        list.append(tuple((URL, throws, verbose, i )))

    # Create pool of workers and perform throws
    sum = 0
    with concurrent.futures.ProcessPoolExecutor() as executor:
        for time in executor.map(do_hammer, list):
            sum = sum + time

    timeAvg = sum / throws * hammers

    output = "TOTAL AVERAGE ELAPSED TIME: " + str(round(timeAvg, 2))
    print(output)

# Main execution

if __name__ == '__main__':
    main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
